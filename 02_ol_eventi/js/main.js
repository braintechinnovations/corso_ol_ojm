window.onload = init;

function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1389880.7546642218, 5144703.885074405],
            zoom: 9,
            maxZoom: 15,
            minZoom: 5,
            rotation: 0.5
        }),
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        target: 'mia-mappa'
    })

    map.on('click', function(evt){
        console.log(evt.coordinate)
    })
}