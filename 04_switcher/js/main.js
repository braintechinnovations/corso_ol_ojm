window.onload = init;

function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1389880.7546642218, 5144703.885074405],
            zoom: 9,
            maxZoom: 15,
            // minZoom: 5,
            rotation: 0.5
        }),
        target: 'mia-mappa'
    })

    const openStreetMapStandard = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true,
        title: 'OSMStandard'
    })

    const stamenToner = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenToner'
    })

    
    const stamenWatercolor = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.'
        }),
        visible: false,
        title: 'StamenWatercolor'
    })
    
    const stamenTerrain = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenTerrain'
    })

    const gruppoLayerMappeBase = new ol.layer.Group({
        layers: [
            openStreetMapStandard,
            stamenToner,
            stamenWatercolor,
            stamenTerrain,
        ]
    }) 

    map.addLayer(gruppoLayerMappeBase)

    // --------------------------------------- SWITCHER -----------------------------------------

    const radioSwitcher = document.querySelectorAll('input[type=radio]')
    
    for(let radioInput of radioSwitcher){
        radioInput.addEventListener('change', function(){
            let titoloMappaSelezionata = this.value;

            gruppoLayerMappeBase.getLayers().forEach(element => {
                let titoloMappaTemp = element.get('title')

                element.setVisible(titoloMappaTemp === titoloMappaSelezionata)
            });
        })
    }
}