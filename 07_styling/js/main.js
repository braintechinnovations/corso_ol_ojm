window.onload = init;

function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1389880.7546642218, 5144703.885074405],
            zoom: 9,
            maxZoom: 15,
            // minZoom: 5,
            rotation: 0.5
        }),
        target: 'mia-mappa'
    })

    const openStreetMapStandard = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true,
        title: 'OSMStandard'
    })

    const stamenToner = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenToner'
    })

    
    const stamenWatercolor = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.'
        }),
        visible: false,
        title: 'StamenWatercolor'
    })
    
    const stamenTerrain = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenTerrain'
    })

    const gruppoLayerMappeBase = new ol.layer.Group({
        layers: [
            openStreetMapStandard,
            stamenToner,
            stamenWatercolor,
            stamenTerrain,
        ]
    }) 

    map.addLayer(gruppoLayerMappeBase)

    //--------------------------- SWITCHER ---------------------------

    const radioSwitcher = document.querySelectorAll('input[type=radio]')
    
    for(let radioInput of radioSwitcher){
        radioInput.addEventListener('change', function(){
            let mappaSelezionata = this.value

            gruppoLayerMappeBase.getLayers().forEach(element => {
                let titoloLivello = element.get('title')

                // if(titoloLivello === mappaSelezionata){
                //     element.setVisible(true)
                // }
                // else{
                //     element.setVisible(false)
                // }

                element.setVisible(titoloLivello === mappaSelezionata)
            });


        })
    }

    // ------------------------ LAYER ------------------------------

    const fillStyle = new ol.style.Fill({
        color: [50, 89, 255, 0.2]
    })

    const strokeStyle = new ol.style.Stroke({
        color: [20, 20, 20, 0.8],
        width: 1.5
    })

    const circleStyle = new ol.style.Circle({
        fill: new ol.style.Fill({
            color: [255, 50, 63, 0.5]
        }),
        radius: 7,
        stroke: strokeStyle
    })

    const zoneRomaGeoJson = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/zoneRoma.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'personalizzato',
        style: new ol.style.Style({
            fill: fillStyle,
            stroke: strokeStyle,
            image: circleStyle
        })
    })
    
    map.addLayer(zoneRomaGeoJson)

    // ------------------------- AGGIUNTA OVERLAY ------------------

    const overlayHtml = document.querySelector('.overlay-container');
    const overlayLayer = new ol.Overlay({
        element: overlayHtml
    })
    map.addOverlay(overlayLayer)

    const spanOverlayNome = document.getElementById('overlay-nome');
    const spanOverlayInfo = document.getElementById('overlay-info');

    map.on('click', function(evt){
        // console.log(evt.pixel)
        overlayLayer.setPosition(undefined)

        map.forEachFeatureAtPixel(evt.pixel, function(feature, layer){
            let nome = feature.get('nome')
            let info = feature.get('infoAddizionali')

            spanOverlayNome.innerHTML = nome;
            spanOverlayInfo.innerHTML = info;

            overlayLayer.setPosition(evt.coordinate)
        }
        // {
        //     layerFilter: function(layer){
        //         return layer.get('title') === "StamenTerrain"
        //     }
        // }
        )
    })

}